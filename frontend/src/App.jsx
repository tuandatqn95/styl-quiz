import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import "./App.css";
import RegisterPage from "./components/RegisterPage";
import VerifyPage from "./components/VerifyPage";
import QuizPage from "./components/QuizPage";
import ErrorPage403 from "./components/ErrorPage/403";
import ErrorPage404 from "./components/ErrorPage/404";
import FinishPage from "./components/FinishPage";
import AdminPage from "./components/AdminPage";
import ResendEmailPage from "./components/ResendEmailPage";

class App extends Component {
    render() {
        return (
            <div className="swal2-in">
                <nav
                    className="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg"
                    color-on-scroll={100}
                    id="sectionsNav"
                />
                <div
                    className="page-header header-filter"
                    style={{
                        backgroundImage: 'url("/assets/img/bg7.jpg")',
                        backgroundSize: "cover",
                        backgroundPosition: "top center",
                        backgroundAttachment: "fixed",
                        height: "100%",
                        minHeight: "100vh"
                    }}
                >
                    <Switch>
                        <Route path="/register" component={RegisterPage} />
                        <Route path="/resend" component={ResendEmailPage} />
                        <Route path="/verify/:userId" component={VerifyPage} />
                        <Route path="/quiz" component={QuizPage} />
                        <Route path="/finish" component={FinishPage} />
                        <Route path="/admin" component={AdminPage} />
                        <Route path="/403" component={ErrorPage403} />
                        <Route path="/404" component={ErrorPage404} />
                        <Redirect path="/" to="/register" />
                    </Switch>
                </div>
            </div>
        );
    }
}

export default App;
