export const API_URL =
    process.env.NODE_ENV === "production"
        ? "/api"
        : "http://localhost:4000/api";

export const majors = ["MOBILE", "SERVER", "EMBEDDED", "HARDWARE"];
export const categories = [
    "GENERAL",
    "MOBILE",
    "SERVER",
    "EMBEDDED",
    "HARDWARE"
];
