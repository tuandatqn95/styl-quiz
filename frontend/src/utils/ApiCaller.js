import axios from "axios";
import { API_URL } from "../constants/config";

export default function callApi(endPoint, method = "GET", body) {
    return axios({
        method: method,
        url: `${API_URL}/${endPoint}`,
        data: body,
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`
        }
    });
}

export function adminApiCaller(endPoint, method = "GET", body, formData) {
    if (formData === true) {
        return callApiFormData(endPoint, method, body);
    }
    return axios({
        method: method,
        url: `${API_URL}/${endPoint}`,
        data: body,
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token_admin")}`
        }
    });
}

function callApiFormData(endPoint, method = "GET", body) {
    const data = new FormData();
    for (const field in body) {
        data.append(field, body[field]);
    }

    return axios({
        method: method,
        url: `${API_URL}/${endPoint}`,
        data: data,
        headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            Authorization: `Bearer ${localStorage.getItem("token_admin")}`
        }
    });
}
