import React, { Component } from "react";

class ErrorPage404 extends Component {
    render() {
        return (
            <div className="container">
                <div className="col-md-4 ml-auto mr-auto">
                    <div className="card card-profile text-center">
                        <div className="card-body ">
                            <h4 className="card-title">Not found</h4>
                            <h5 className="card-description">
                                Cannot find this page.
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ErrorPage404;
