import React, { Component } from "react";

class ErrorPage403 extends Component {
    render() {
        return (
            <div className="container">
                <div className="col-md-4 ml-auto mr-auto">
                    <div className="card card-profile text-center">
                        <div className="card-body ">
                            <h4 className="card-title">Access denided</h4>
                            <h5 className="card-description">
                                You don't have permission to access this page.
                            </h5>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ErrorPage403;
