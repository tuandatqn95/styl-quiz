import React, { Component } from "react";
import apiCaller from "../../utils/ApiCaller";
import { ClipLoader } from "react-spinners";

class FinishPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            success: false,
            error: false,
            result: ""
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.checkSubmitedAnswer(nextProps.submited)) {
            apiCaller(`result`, "POST")
                .then(response => {
                    console.log(response.data);

                    this.setState({
                        result: response.data.result,
                        success: true
                    });
                })
                .catch(err => {
                    this.setState({ error: true });
                });
        }
    }

    checkSubmitedAnswer(submited) {
        for (let answer in submited) {
            if (submited[answer] === false) {
                return false;
            }
        }
        return true;
    }

    render() {
        const { success, error, result } = this.state;
        return (
            <div className="container">
                <div className="col-md-4 ml-auto mr-auto">
                    <div className="card card-profile text-center">
                        <div className="card-body text-center">
                            {" "}
                            <ClipLoader
                                sizeUnit={"px"}
                                size={150}
                                color={"#123abc"}
                                loading={!success && !error}
                            />
                        </div>
                        {success && [
                            <div
                                key="card-body"
                                className="card-body text-center"
                            >
                                <img src="/assets/img/logo-styl.png" alt="" />
                                <h4 className="card-title">Congratulation!</h4>
                                <h1>
                                    <b>{result}</b>
                                </h1>
                            </div>,
                            <div
                                key="alert-success"
                                className="alert alert-success"
                            >
                                <div className="container text-justify">
                                    <div className="alert-icon">
                                        <i className="material-icons">check</i>
                                    </div>
                                    Please check your email for the result.
                                </div>
                            </div>
                        ]}
                        {error && [
                            <div key="card-body" className="card-body ">
                                <h4 className="card-title">Error!</h4>
                            </div>,
                            <div
                                key="alert-danger"
                                className="alert alert-danger"
                            >
                                <div className="container text-justify">
                                    <div className="alert-icon">
                                        <i className="material-icons">
                                            error_outline
                                        </i>
                                    </div>
                                    Cannot submit your result. Please contact to
                                    our staff for the explanation.
                                </div>
                            </div>
                        ]}
                    </div>
                </div>
            </div>
        );
    }
}

export default FinishPage;
