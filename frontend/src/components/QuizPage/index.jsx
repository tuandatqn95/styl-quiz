import React, { Component } from "react";
import Question from "../Question";
import FinishPage from "../FinishPage";
import { ClipLoader } from "react-spinners";
import apiCaller from "../../utils/ApiCaller";

const Questions = props => {
    const {
        total,
        current,
        questions,
        ready,
        fromVerify,
        nextQuestion
    } = props;
    if (!fromVerify || !ready || !questions) {
        return null;
    }

    return questions.map((question, index) => {
        if (index + 1 !== current) {
            return null;
        }
        return (
            <Question
                key={question._id}
                question={question.question}
                answerId={question._id}
                total={total}
                current={index + 1}
                nextQuestion={nextQuestion}
            />
        );
    });
};

class QuizPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ready: false,
            error: false,
            fromVerify: false,
            total: 0,
            current: 1,
            questions: [],
            submited: []
        };
    }

    componentWillMount() {
        const { state } = this.props.location;
        if (state && state.fromVerify) {
            this.setState({ fromVerify: true });
        } else {
            this.setState({ error: true });
        }
    }

    componentDidMount() {
        if (this.state.fromVerify)
            apiCaller(`question`, "GET")
                .then(response => {
                    const { questions, current, total } = response.data;
                    this.setState({
                        ready: true,
                        questions: Array(current - 1).concat(questions),
                        current: current,
                        total: total
                    });
                })
                .catch(err => {
                    this.setState({
                        error: true
                    });
                    console.log(err);
                });
    }

    nextQuestion = answer => {
        const { id, choice } = answer;

        this.submittingQuestion(id);
        if (id && choice) {
            apiCaller(`answer/${id}/${choice}`, "PUT")
                .then(response => {
                    this.submitedQuestion(id);
                })
                .catch(err => {
                    this.submitedQuestion(id);
                });
        } else {
            this.submitedQuestion(id);
        }
    };

    submittingQuestion = id => {
        const { submited, current } = this.state;
        this.setState({
            current: current + 1,
            submited: {
                ...submited,
                [id]: false
            }
        });
    };

    submitedQuestion = id => {
        const { submited } = this.state;
        this.setState({
            submited: {
                ...submited,
                [id]: true
            }
        });
    };

    render() {
        const {
            ready,
            error,
            fromVerify,
            questions,
            total,
            current,
            submited
        } = this.state;
        if (current > total) {
            return <FinishPage submited={submited} />;
        }
        return (
            <div className="container">
                <div className="row">
                    <div className="col-lg-4 col-md-6 ml-auto mr-auto">
                        <div className="card">
                            <div className="card-body text-center">
                                <ClipLoader
                                    sizeUnit={"px"}
                                    size={150}
                                    color={"#123abc"}
                                    loading={!ready && !error}
                                />
                            </div>

                            <div className="alert alert-danger" hidden={!error}>
                                <div className="container">
                                    <div className="alert-icon">
                                        <i className="material-icons">
                                            error_outline
                                        </i>
                                    </div>
                                    <b>Error:</b> You cannot play. Please
                                    contact our staff.
                                </div>
                            </div>
                            <div className="card-body">
                                <Questions
                                    ready={ready}
                                    fromVerify={fromVerify}
                                    questions={questions}
                                    total={total}
                                    current={current}
                                    nextQuestion={this.nextQuestion}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default QuizPage;
