import React, { Component } from "react";
import apiCaller from "../../utils/ApiCaller";

const emailPattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,}$";

const emailRegex = new RegExp(emailPattern);

class ResendEmailPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",

            success: false,
            error: "",
            invalid: true,
            submiting: false,
            focused: {}
        };
    }

    onHandleChange = e => {
        let name = e.target.name;
        let value = e.target.value;
        this.setState(
            {
                [name]: value
            },
            () => {
                this.toggleButton();
            }
        );
    };

    onFocus = e => {
        let name = e.target.name;
        this.setState({
            focused: {
                ...this.state.focused,
                [name]: true
            }
        });
    };

    onHandleError = err => {
        this.setState({
            error: err.response.data.message || err.response.statusText,
            submiting: false
        });
    };

    onHandleSuccess = data => {
        console.log(data);
        this.setState({
            success: true,
            submiting: false,
            error: ""
        });
    };

    onSubmitForm = e => {
        e.preventDefault();

        if (this.validate(this.state)) {
            const { email } = this.state;
            const user = {
                email
            };
            this.setState({ submiting: true });
            apiCaller("resend", "POST", user)
                .then(res => this.onHandleSuccess(res.data))
                .catch(err => this.onHandleError(err));
        }
    };

    toggleButton = () => {
        this.setState({ invalid: !this.validate(this.state) });
    };

    validate = user => {
        const { email } = user;

        if (!email) {
            return false;
        }
        if (!emailRegex.test(email)) {
            return false;
        }

        return true;
    };

    render() {
        const {
            email,

            success,
            error,
            invalid,
            submiting,
            focused
        } = this.state;
        return (
            <div className="container">
                <div className="row">
                    <div className="col-lg-4 col-md-6 ml-auto mr-auto">
                        <div className="card">
                            <div
                                className="card-header card-header-info text-center"
                                style={{ marginBottom: 30 }}
                            >
                                <img src="/assets/img/logo-styl.png" alt="" />
                                <h4 className="card-title">
                                    Resend Verification Email
                                </h4>
                            </div>
                            <div className="clearfix" />
                            <div
                                className="alert alert-success"
                                hidden={!success}
                            >
                                <div className="container">
                                    <div className="alert-icon">
                                        <i className="material-icons">check</i>
                                    </div>
                                    Please check your email to join our quiz and
                                    earn reward.
                                </div>
                            </div>
                            <div
                                className="alert alert-warning"
                                hidden={!error}
                            >
                                <div className="container">
                                    <div className="alert-icon">
                                        <i className="material-icons">
                                            warning
                                        </i>
                                    </div>
                                    <button
                                        type="button"
                                        className="close"
                                        data-dismiss="alert"
                                        aria-label="Close"
                                    >
                                        <span aria-hidden="true">
                                            <i className="material-icons">
                                                clear
                                            </i>
                                        </span>
                                    </button>
                                    <b>Error:</b> {error}
                                </div>
                            </div>
                            <form
                                className="form"
                                onSubmit={this.onSubmitForm}
                                hidden={success}
                            >
                                <div className="card-body">
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">
                                                <i className="material-icons">
                                                    mail
                                                </i>
                                            </span>
                                        </div>

                                        <input
                                            placeholder="Email..."
                                            className="form-control"
                                            required
                                            pattern={emailPattern}
                                            name="email"
                                            value={email}
                                            onChange={this.onHandleChange}
                                            onFocus={this.onFocus}
                                        />
                                        <span
                                            className={`form-control-feedback ${
                                                focused.email ? "focused" : ""
                                            }`}
                                        >
                                            <i className="material-icons invalid">
                                                clear
                                            </i>
                                            <i className="material-icons valid">
                                                done
                                            </i>
                                        </span>
                                    </div>
                                </div>
                                <div className="footer text-center">
                                    <input
                                        type="submit"
                                        className={`btn btn-success btn-wd btn-md ${
                                            invalid ? "btn-default" : "btn-info"
                                        }`}
                                        value="Send"
                                        disabled={invalid || submiting}
                                    />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ResendEmailPage;
