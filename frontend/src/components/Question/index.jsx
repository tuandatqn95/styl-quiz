import React, { Component } from "react";
import apiCaller from "../../utils/ApiCaller";
import { ClipLoader } from "react-spinners";
const Choices = props => {
    const { choices, selected, onCheckChange } = props;
    return choices.map(choice => {
        return (
            <div key={choice._id} className="form-check">
                <label className="form-check-label">
                    <input
                        className="form-check-input"
                        type="checkbox"
                        checked={choice._id === selected}
                        onClick={() => onCheckChange(choice._id)}
                    />
                    {choice.choice}
                    <span className="form-check-sign">
                        <span className="check" />
                    </span>
                </label>
            </div>
        );
    });
};

class Question extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: null,
            timeLeft: 0,
            submitting: false
        };
    }

    componentDidMount() {
        const id = this.props.answerId;
        apiCaller(`answer/${id}`, "PUT")
            .then(response => {
                const { question } = response.data;
                this.setState({
                    question,
                    timeLeft: question.timeLimit
                });
                this.countdown = setInterval(() => {
                    this.calculateCountdown();
                }, 1000);
            })
            .catch(err => {
                console.log(err);
            });
    }

    calculateCountdown = () => {
        let { timeLeft } = this.state;
        if (timeLeft > 0) {
            timeLeft--;
            this.setState({ timeLeft });
            if (timeLeft === 0) {
                this.nextQuestion();
            }
        }
    };

    componentWillUnmount() {
        clearInterval(this.countdown);
    }

    nextQuestion = () => {
        if (this.state.submitting) {
            return;
        }
        this.setState({ submitting: true });
        this.props.nextQuestion({
            id: this.props.answerId,
            choice: this.state.selected
        });
    };

    onCheckChange = choiceId => {
        this.setState({ selected: choiceId });
    };

    render() {
        const { current, total } = this.props;
        const { selected, timeLeft, submitting, question } = this.state;
        return (
            <div className="card card-question">
                <div className="card-header card-header-info text-center">
                    <h3 className="card-title">
                        Question {current}/{total}
                        <button
                            className="btn btn-danger btn-fab btn-round pull-right"
                            style={{ marginRight: 20 }}
                        >
                            {timeLeft}
                            <div className="ripple-container" />
                        </button>
                    </h3>
                </div>
                <div className="card-body text-center" hidden={question}>
                    <ClipLoader
                        sizeUnit={"px"}
                        size={150}
                        color={"#123abc"}
                        loading={!question}
                    />
                </div>

                {question && [
                    <div key="card-body" className="card-body">
                        <h4 className="card-description text-justify">
                            {question.question}
                        </h4>
                        {question.image && (
                            <img src={question.image} width="100%" alt="" />
                        )}
                        <Choices
                            choices={question.choices}
                            selected={selected}
                            onCheckChange={this.onCheckChange}
                        />
                    </div>,
                    <div key="clearfix" className="clearfix" />,
                    <div
                        key="card-footer"
                        className="card-footer justify-content-center"
                    >
                        <button
                            className="btn btn-success btn-round"
                            disabled={submitting}
                            onClick={this.nextQuestion}
                        >
                            {total === current ? "Finish" : "Confirm"}
                        </button>
                    </div>
                ]}
            </div>
        );
    }
}

export default Question;
