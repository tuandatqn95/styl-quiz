import React, { Component } from "react";
import apiCaller from "../../utils/ApiCaller";
import { majors } from "./../../constants/config";
import { Link } from "react-router-dom";
import YearMonthSelector from "react-year-month-selector";

const emailPattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,}$";
const phonePattern = "(0[3|5|7|8|9]|01[2|6|8|9])[0-9]{8}\\b";
const monthYearPattern = "(0[1-9]|1[0-2])-[2-9][0-9]{3}";
const emailRegex = new RegExp(emailPattern);
const phoneRegex = new RegExp(phonePattern);
const monthYearRegex = new RegExp(monthYearPattern);

const Majors = props => {
    return props.majors.map(major => {
        return (
            <a
                key={major}
                href="#answer"
                className="dropdown-item"
                onClick={() => props.onSelectMajor(major)}
            >
                {major}
            </a>
        );
    });
};

const MajorSelect = props => {
    return (
        <div className="dropdown-menu">
            <h6 className="dropdown-header">Select Major</h6>
            <Majors majors={props.majors} onSelectMajor={props.onSelectMajor} />
        </div>
    );
};

class RegisterPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            phone: "",
            university: "",
            faculty: "",
            major: "",
            expectedGraduation: "",
            success: false,
            error: "",
            invalid: true,
            submiting: false,
            focused: {},
            openMonthPicker: false
        };
    }

    onHandleChange = e => {
        let name = e.target.name;
        let value = e.target.value;
        this.setState(
            {
                [name]: value
            },
            () => {
                this.toggleButton();
            }
        );
    };

    onSelectMonth = value => {
        this.setState(
            {
                expectedGraduation: value
            },
            () => {
                this.toggleButton();
            }
        );
    };

    onFocus = e => {
        let name = e.target.name;
        this.setState({
            focused: {
                ...this.state.focused,
                [name]: true
            }
        });
    };

    onSelectMajor = major => {
        this.setState({ major }, () => {
            this.toggleButton();
        });
    };

    onHandleError = err => {
        this.setState({
            error: err.response.data.message || err.response.statusText,
            submiting: false
        });
    };

    onHandleSuccess = data => {
        console.log(data);
        this.setState({
            success: true,
            submiting: false,
            error: ""
        });
    };

    onFocusMonthPicker = () => {
        this.setState({
            openMonthPicker: true,
            focused: {
                ...this.state.focused,
                expectedGraduation: true
            }
        });
    };

    onHandleChangeMonthPicker = (year, month) => {
        const monthStr = month + 1 < 10 ? `0${month + 1}` : `${month + 1}`;
        const result = `${monthStr}-${year}`;
        this.setState(
            {
                expectedGraduation: result
            },
            () => this.toggleButton()
        );
    };

    onHandleCloseMonthPicker = () => {
        this.setState({ openMonthPicker: false });
    };

    onSubmitForm = e => {
        e.preventDefault();

        if (this.validate(this.state)) {
            const {
                name,
                email,
                phone,
                university,
                faculty,
                major,
                expectedGraduation
            } = this.state;
            const user = {
                name,
                email,
                phone,
                university,
                faculty,
                major,
                expectedGraduation
            };
            this.setState({ submiting: true });
            apiCaller("register", "POST", user)
                .then(res => this.onHandleSuccess(res.data))
                .catch(err => this.onHandleError(err));
        }
    };

    toggleButton = () => {
        this.setState({ invalid: !this.validate(this.state) });
    };

    validate = user => {
        const {
            name,
            email,
            phone,
            university,
            faculty,
            major,
            expectedGraduation
        } = user;
        if (!name) {
            return false;
        }

        if (!email) {
            return false;
        }
        if (!emailRegex.test(email)) return false;

        if (!phone) {
            return false;
        }
        if (!phoneRegex.test(phone)) {
            return false;
        }
        if (!university) {
            return false;
        }
        if (!faculty) {
            return false;
        }
        if (!major) {
            return false;
        }
        if (!expectedGraduation) {
            return false;
        }
        if (!monthYearRegex.test(expectedGraduation)) {
            return false;
        }
        return true;
    };

    render() {
        const {
            name,
            email,
            phone,
            university,
            faculty,
            major,
            expectedGraduation,
            success,
            error,
            invalid,
            submiting,
            focused,
            openMonthPicker
        } = this.state;
        return (
            <div className="container">
                <div className="row">
                    <div className="col-lg-4 col-md-6 ml-auto mr-auto">
                        <div className="card card-login">
                            <div className="card-header card-header-info text-center">
                                <img src="/assets/img/logo-styl.png" alt="" />
                                <h4 className="card-title">
                                    Register {success && "Successful"}
                                </h4>
                            </div>
                            <div
                                className="alert alert-success"
                                hidden={!success}
                            >
                                <div className="container">
                                    <div className="alert-icon">
                                        <i className="material-icons">check</i>
                                    </div>
                                    Please check your email to join our quiz and
                                    earn reward.
                                    <br />
                                </div>
                            </div>
                            <div className="alert" hidden={!success}>
                                <div className="container text-center">
                                    <Link to="/resend">
                                        Click here if you haven't received
                                        confirmation email!
                                    </Link>
                                    <br />
                                </div>
                            </div>
                            <div
                                className="alert alert-warning"
                                hidden={!error}
                            >
                                <div className="container">
                                    <div className="alert-icon">
                                        <i className="material-icons">
                                            warning
                                        </i>
                                    </div>
                                    <button
                                        type="button"
                                        className="close"
                                        data-dismiss="alert"
                                        aria-label="Close"
                                    >
                                        <span aria-hidden="true">
                                            <i className="material-icons">
                                                clear
                                            </i>
                                        </span>
                                    </button>
                                    <b>Error:</b> {error}
                                </div>
                            </div>
                            <form
                                className="form"
                                onSubmit={this.onSubmitForm}
                                hidden={success}
                            >
                                <div className="card-body">
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">
                                                <i className="material-icons">
                                                    face
                                                </i>
                                            </span>
                                        </div>
                                        <input
                                            className="form-control"
                                            placeholder="Full Name..."
                                            required
                                            type="text"
                                            name="name"
                                            value={name}
                                            onChange={this.onHandleChange}
                                            onFocus={this.onFocus}
                                        />
                                        <span
                                            className={`form-control-feedback ${
                                                focused.name ? "focused" : ""
                                            }`}
                                        >
                                            <i className="material-icons invalid">
                                                clear
                                            </i>
                                            <i className="material-icons valid">
                                                done
                                            </i>
                                        </span>
                                    </div>

                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">
                                                <i className="material-icons">
                                                    mail
                                                </i>
                                            </span>
                                        </div>

                                        <input
                                            placeholder="Email..."
                                            className="form-control"
                                            required
                                            pattern={emailPattern}
                                            name="email"
                                            value={email}
                                            onChange={this.onHandleChange}
                                            onFocus={this.onFocus}
                                        />
                                        <span
                                            className={`form-control-feedback ${
                                                focused.email ? "focused" : ""
                                            }`}
                                        >
                                            <i className="material-icons invalid">
                                                clear
                                            </i>
                                            <i className="material-icons valid">
                                                done
                                            </i>
                                        </span>
                                        <span className="custom-help">
                                            <em>
                                                You will receive a confirmation
                                                email
                                            </em>
                                        </span>
                                    </div>

                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">
                                                <i className="material-icons">
                                                    phone
                                                </i>
                                            </span>
                                        </div>
                                        <input
                                            className="form-control"
                                            placeholder="Phone number..."
                                            required
                                            pattern={phonePattern}
                                            type="text"
                                            name="phone"
                                            value={phone}
                                            onChange={this.onHandleChange}
                                            onFocus={this.onFocus}
                                        />
                                        <span
                                            className={`form-control-feedback ${
                                                focused.phone ? "focused" : ""
                                            }`}
                                        >
                                            <i className="material-icons invalid">
                                                clear
                                            </i>
                                            <i className="material-icons valid">
                                                done
                                            </i>
                                        </span>
                                    </div>
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">
                                                <i className="material-icons">
                                                    location_city
                                                </i>
                                            </span>
                                        </div>
                                        <input
                                            className="form-control"
                                            placeholder="University..."
                                            required={true}
                                            type="text"
                                            name="university"
                                            value={university}
                                            onChange={this.onHandleChange}
                                            onFocus={this.onFocus}
                                            list="universities"
                                        />

                                        <span
                                            className={`form-control-feedback ${
                                                focused.university
                                                    ? "focused"
                                                    : ""
                                            }`}
                                        >
                                            <i className="material-icons invalid">
                                                clear
                                            </i>
                                            <i className="material-icons valid">
                                                done
                                            </i>
                                        </span>
                                        <datalist id="universities">
                                            <option value="BKU - Bach Khoa University" />
                                            <option value="HCMUS - University of Science" />
                                            <option value="UIT - University of Infomation Technology" />
                                            <option value="HCMUTE - University of Technology and Education" />
                                        </datalist>
                                    </div>
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">
                                                <i className="material-icons">
                                                    account_balance
                                                </i>
                                            </span>
                                        </div>
                                        <input
                                            className="form-control"
                                            placeholder="Faculty..."
                                            required
                                            type="text"
                                            name="faculty"
                                            value={faculty}
                                            onChange={this.onHandleChange}
                                            onFocus={this.onFocus}
                                        />
                                        <span
                                            className={`form-control-feedback ${
                                                focused.faculty ? "focused" : ""
                                            }`}
                                        >
                                            <i className="material-icons invalid">
                                                clear
                                            </i>
                                            <i className="material-icons valid">
                                                done
                                            </i>
                                        </span>
                                    </div>
                                    <span className="bmd-form-group">
                                        <div className="input-group">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text">
                                                    <i className="material-icons">
                                                        local_library
                                                    </i>
                                                </span>
                                            </div>

                                            <button
                                                className="dropdown-toggle form-control"
                                                data-toggle="dropdown"
                                                style={{
                                                    textAlign: "left"
                                                }}
                                            >
                                                {major ? major : "Major..."}
                                            </button>
                                            <MajorSelect
                                                majors={majors}
                                                onSelectMajor={
                                                    this.onSelectMajor
                                                }
                                            />
                                        </div>
                                    </span>

                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text">
                                                <i className="material-icons">
                                                    date_range
                                                </i>
                                            </span>
                                        </div>

                                        <input
                                            className="form-control"
                                            placeholder="Expected graduation..."
                                            required
                                            type="text"
                                            name="expectedGraduation"
                                            pattern={monthYearPattern}
                                            value={expectedGraduation}
                                            onFocus={this.onFocusMonthPicker}
                                        />

                                        <span
                                            className={`form-control-feedback ${
                                                focused.expectedGraduation
                                                    ? "focused"
                                                    : ""
                                            }`}
                                        >
                                            <i className="material-icons invalid">
                                                clear
                                            </i>
                                            <i className="material-icons valid">
                                                done
                                            </i>
                                        </span>
                                    </div>
                                </div>
                                <YearMonthSelector
                                    onChange={this.onHandleChangeMonthPicker}
                                    open={openMonthPicker}
                                    onClose={this.onHandleCloseMonthPicker}
                                />
                                <div className="footer text-center">
                                    <input
                                        type="submit"
                                        className={`btn btn-success btn-wd btn-md ${
                                            invalid ? "btn-default" : "btn-info"
                                        }`}
                                        value="Get Started"
                                        disabled={invalid || submiting}
                                    />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default RegisterPage;
