import React, { Component } from "react";
import { adminApiCaller } from "../../utils/ApiCaller";
import { categories } from "../../constants/config";

const Categories = props => {
    return props.categories.map(category => {
        return (
            <a
                key={category}
                href="#answer"
                className="dropdown-item"
                onClick={() => props.onSelectCategory(category)}
            >
                {category}
            </a>
        );
    });
};
const CategorySelect = props => {
    return (
        <div className="dropdown-menu">
            <h6 className="dropdown-header">Select Category</h6>
            <Categories
                categories={props.categories}
                onSelectCategory={props.onSelectCategory}
            />
        </div>
    );
};

class AddQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            category: "GENERAL",
            question: "",
            choices: [],
            timeLimit: 0,
            image: null,
            previewImage: "",
            editing: false
        };
    }

    componentWillMount() {
        console.log("props:", this.props);
        console.log("params:", this.props.match.params);
        console.log(this.props.location.state);
        if (this.props.match.path === "/admin/question/edit/:id") {
            const id = this.props.match.params.id;

            adminApiCaller("admin/question/" + id, "GET")
                .then(response => {
                    this.onResetForm();

                    const { question } = response.data;

                    question.choices = question.choices.map(choice => {
                        return {
                            key: choice._id,
                            choice: choice.choice,
                            right: choice._id === question.answer
                        };
                    });
                    this.setState({
                        category: question.category,
                        question: question.question,
                        choices: question.choices,
                        timeLimit: question.timeLimit,
                        image: null,
                        previewImage: question.image,
                        editing: true
                    });
                })
                .catch(err => {
                    console.log(err);
                });
        }
    }

    onResetForm = () => {
        this.setState({
            category: "GENERAL",
            question: "",
            choices: [],
            timeLimit: 30,
            image: null,
            previewImage: ""
        });
    };

    onHandleChange = e => {
        const target = e.target;
        const name = target.name;
        const value = target.value;

        if (target.files) {
            const reader = new FileReader();
            const file = target.files[0];
            reader.onloadend = () => {
                this.setState({
                    image: file,
                    previewImage: reader.result
                });
            };
            reader.readAsDataURL(file);
        } else {
            this.setState({
                [name]: value
            });
        }
    };

    onHandleChangeChoices = e => {
        let key = e.target.name;
        let value = e.target.value;
        let { choices } = this.state;
        const index = choices.findIndex(choice => choice.key === key);
        if (e.target.type === "checkbox") {
            choices = choices.map(choice => {
                return {
                    ...choice,
                    right: choice.key === key
                };
            });
        } else {
            choices[index].choice = value;
        }

        this.setState({
            choices
        });
    };

    onSubmitForm = e => {
        e.preventDefault();
        const {
            question,
            category,
            choices,
            timeLimit,
            image,
            editing
        } = this.state;
        const jsonData = {
            question,
            category,
            choices,
            timeLimit
        };
        const data = {
            data: JSON.stringify(jsonData),
            image
        };
        if (editing) {
            const id = this.props.match.params.id;
            adminApiCaller("admin/question/" + id, "PUT", data, true)
                .then(response => {
                    console.log(response);
                    this.props.history.push("/admin/question/");
                })
                .catch(err => {
                    console.log(err);
                });
        } else {
            adminApiCaller("admin/question", "POST", data, true)
                .then(response => {
                    console.log(response);
                    this.onResetForm();
                })
                .catch(err => {
                    console.log(err);
                });
        }
    };

    onSelectCategory = category => {
        this.setState({ category });
    };

    onAddChoice = e => {
        e.preventDefault();
        const { choices } = this.state;
        const key = new Date().getTime().toString();
        this.setState({
            choices: [...choices, { key, choice: "", right: false }]
        });
    };

    onDeleteChoice = e => {
        const key = e.target.id;

        e.preventDefault();
        let { choices } = this.state;
        const index = choices.findIndex(choice => choice.key === key);
        choices.splice(index, 1);

        this.setState({ choices });
    };

    render() {
        const {
            question,
            category,
            choices,
            timeLimit,
            previewImage,
            editing
        } = this.state;
        return (
            <div className="tab-pane active">
                <div className="card-body">
                    <form>
                        <div className="row">
                            <div className="col-md-12">
                                <div
                                    className="form-group"
                                    style={{
                                        textAlign: "left"
                                    }}
                                >
                                    <label className="bmd-label-floating">
                                        Question
                                    </label>
                                    <input
                                        className="form-control"
                                        type="text"
                                        name="question"
                                        value={question}
                                        onChange={this.onHandleChange}
                                    />
                                </div>
                            </div>
                        </div>
                        <div
                            className="row"
                            style={{
                                textAlign: "left"
                            }}
                        >
                            <div className="col-md-12">
                                <div className="row">
                                    <div className="col-md-12">
                                        <label>
                                            <u>Choices</u>
                                        </label>
                                    </div>
                                </div>

                                {choices.map((choice, index) => {
                                    return (
                                        <div
                                            key={index}
                                            className="row"
                                            style={{
                                                textAlign: "left"
                                            }}
                                        >
                                            <div className="col-xs-8 col-sm-8 col-md-10">
                                                <label
                                                    className="bmd-label-floating"
                                                    style={{
                                                        position: "absolute",
                                                        top: 9
                                                    }}
                                                >
                                                    {`Choice ${index + 1}:`}
                                                </label>
                                                <input
                                                    className="form-control"
                                                    type="text"
                                                    name={choice.key}
                                                    value={choice.choice}
                                                    onChange={
                                                        this
                                                            .onHandleChangeChoices
                                                    }
                                                    style={{ paddingLeft: 80 }}
                                                />
                                            </div>

                                            <div className="col-xs-2 col-sm-2 col-md-1">
                                                <div className="form-check">
                                                    <label className="form-check-label">
                                                        <input
                                                            className="form-check-input"
                                                            type="checkbox"
                                                            name={choice.key}
                                                            checked={
                                                                choice.right
                                                            }
                                                            onChange={
                                                                this
                                                                    .onHandleChangeChoices
                                                            }
                                                        />
                                                        <span className="form-check-sign">
                                                            <span className="check" />
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div className="col-xs-2 col-sm-2 col-md-1">
                                                <button
                                                    id={choice.key}
                                                    title="Delete"
                                                    className="btn btn-danger btn-link btn-just-icon btn-sm"
                                                    onClick={
                                                        this.onDeleteChoice
                                                    }
                                                >
                                                    <i className="material-icons">
                                                        close
                                                    </i>
                                                </button>
                                            </div>
                                        </div>
                                    );
                                })}
                                <div className="row">
                                    <div className="col-md-12">
                                        <button
                                            className="btn btn-success btn-sm pull-left"
                                            type="none"
                                            onClick={this.onAddChoice}
                                        >
                                            Add Choice
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div className="row">
                            <div className="col-md-8">
                                <img
                                    src={
                                        previewImage ||
                                        "/assets/img/default-img.gif"
                                    }
                                    alt=""
                                    width="100%"
                                />
                            </div>
                            <div className="col-md-4">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="form-group bmd-form-group is-filled">
                                            <label className="bmd-label-floating">
                                                Category
                                            </label>
                                            <button
                                                className="dropdown-toggle form-control"
                                                data-toggle="dropdown"
                                                style={{
                                                    textAlign: "left"
                                                }}
                                            >
                                                {category}
                                            </button>
                                            <CategorySelect
                                                categories={categories}
                                                onSelectCategory={
                                                    this.onSelectCategory
                                                }
                                            />
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-12">
                                        <div
                                            className="form-group"
                                            style={{
                                                textAlign: "left"
                                            }}
                                        >
                                            <label className="bmd-label-floating">
                                                Time Limit
                                            </label>
                                            <input
                                                className="form-control"
                                                type="number"
                                                name="timeLimit"
                                                value={timeLimit}
                                                onChange={this.onHandleChange}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <span className="btn btn-raised btn-round btn-default btn-file">
                                            <span className="fileinput-new">
                                                Select image
                                            </span>

                                            <input
                                                name="image"
                                                type="file"
                                                onChange={this.onHandleChange}
                                            />
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr />
                        <button
                            type="submit"
                            className="btn btn-success pull-right"
                            onClick={this.onSubmitForm}
                        >
                            {editing ? "Save" : "Add"} Question
                        </button>
                        <div className="clearfix" />
                    </form>
                </div>
            </div>
        );
    }
}

export default AddQuestion;
