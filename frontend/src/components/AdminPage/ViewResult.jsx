import React, { Component } from "react";
import { adminApiCaller } from "../../utils/ApiCaller";
import { ClipLoader } from "react-spinners";
import moment from "moment";

const Results = props => {
    return props.results.map((result, index) => (
        <tr key={result._id}>
            <td className="text-center">{index + 1}</td>
            <td className="text-left">{result.user.name}</td>
            <td>{result.user.email}</td>
            <td>{result.user.phone}</td>
            <td>{result.user.university}</td>
            <td>{result.user.faculty}</td>
            <td>{result.user.major}</td>
            <td>{result.user.expectedGraduation}</td>
            <td>{moment(result.user.createdAt).format("DD/MM/YYYY")}</td>
            <td className="text-right">{result.numberOfCorrect}</td>
            <td className="td-actions text-right">
                {/* <button
                    type="button"
                    title="View Detail"
                    className="btn btn-info btn-link btn-just-icon btn-sm"
                >
                    <i className="material-icons">person</i>
                </button> */}
            </td>
        </tr>
    ));
};

class ViewResult extends Component {
    constructor(props) {
        super(props);
        this.state = {
            results: [],
            loading: true
        };
    }

    componentDidMount() {
        this.fetchResults();
    }

    fetchResults = () => {
        this.setState({ loading: true });
        adminApiCaller("admin/result", "GET")
            .then(response => {
                const { results } = response.data;
                this.setState({
                    results,
                    loading: false
                });
            })
            .catch(err => {
                console.log(err);
            });
    };

    onHandleChange = e => {
        let name = e.target.name;
        let value = e.target.value;
        this.setState({
            [name]: value
        });
    };

    render() {
        const { results, loading } = this.state;
        return (
            <div className="tab-pane active">
                <div className="card-body">
                    <ClipLoader
                        sizeUnit={"px"}
                        size={150}
                        color={"#123abc"}
                        loading={loading}
                    />
                    <div className="table-responsive" hidden={loading}>
                        <table className="table">
                            <thead>
                                <tr>
                                    <th className="text-center">#</th>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>University</th>
                                    <th>Faculty</th>
                                    <th>Major</th>
                                    <th>Expected Graduation</th>
                                    <th>Created At</th>
                                    <th className="text-right">Right Answer</th>
                                    <th />
                                </tr>
                            </thead>
                            <tbody>
                                <Results results={results} />
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default ViewResult;
