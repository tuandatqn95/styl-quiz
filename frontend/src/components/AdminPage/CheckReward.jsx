import React, { Component } from "react";
import { adminApiCaller } from "../../utils/ApiCaller";

class CheckReward extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reward: null,
            giftCode: ""
        };
    }

    onHandleChange = e => {
        let name = e.target.name;
        let value = e.target.value;
        this.setState({
            [name]: value
        });
    };

    onSearchReward = e => {
        e.preventDefault();
        const { giftCode } = this.state;
        if (giftCode) {
            adminApiCaller(`reward/${giftCode}`, "GET")
                .then(response => {
                    const reward = response.data;

                    this.setState({
                        reward,
                        error: false
                    });
                })
                .catch(err => {
                    this.setState({
                        reward: null,
                        error: true
                    });
                    console.log(err);
                });
        }
    };

    onConfirmReward = () => {
        const { giftCode } = this.state.reward;
        adminApiCaller(`reward/${giftCode}`, "PUT")
            .then(response => {
                const reward = response.data;
                this.setState({
                    reward
                });
            })
            .catch(err => {
                this.setState({
                    error: true
                });
                console.log(err);
            });
    };

    render() {
        const { reward, giftCode, error } = this.state;
        return (
            <div className="tab-pane active">
                <div className="card-body card-search">
                    <form
                        className="form-inline"
                        onSubmit={this.onSearchReward}
                    >
                        <div className="form-group" style={{ padding: 0 }}>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Search"
                                name="giftCode"
                                value={giftCode}
                                onChange={this.onHandleChange}
                            />
                        </div>
                        <button
                            type="submit"
                            className="btn btn-success btn-raised btn-fab btn-round"
                        >
                            <i className="material-icons">search</i>
                        </button>
                    </form>
                </div>
                <div className="alert alert-danger" hidden={!error}>
                    <div className="container">
                        <div className="alert-icon">
                            <i className="material-icons">error_outline</i>
                        </div>
                        <b>Error:</b> Reward not found!
                    </div>
                </div>
                {reward != null && (
                    <div class="table-responsive" style={{ textAlign: "left" }}>
                        <table class="table table-hover">
                            <tbody>
                                <tr>
                                    <td>
                                        <b>Name:</b>
                                    </td>
                                    <td>{reward.user && reward.user.name}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Email:</b>
                                    </td>
                                    <td>{reward.user && reward.user.email}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Phone:</b>
                                    </td>
                                    <td>{reward.user && reward.user.phone}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Correct:</b>
                                    </td>
                                    <td>{reward.numberOfCorrect}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Reward type:</b>
                                    </td>
                                    <td>{reward.rewardType}</td>
                                </tr>
                            </tbody>
                        </table>

                        <div className="card-body text-center">
                            {reward.received ? (
                                <button className="btn btn-danger btn-round">
                                    Already
                                </button>
                            ) : (
                                <button
                                    className="btn btn-success btn-round"
                                    onClick={this.onConfirmReward}
                                >
                                    Confirm
                                </button>
                            )}
                        </div>
                    </div>
                )}
            </div>
        );
    }
}

export default CheckReward;
