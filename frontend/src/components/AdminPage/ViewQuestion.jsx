import React, { Component } from "react";
import { adminApiCaller } from "../../utils/ApiCaller";

import { categories } from "./../../constants/config";
import { ClipLoader } from "react-spinners";

const Categories = props => {
    return props.categories.map(category => {
        return (
            <a
                key={category}
                href="#answer"
                className="dropdown-item"
                onClick={() => props.onSelectCategory(category)}
            >
                {category}
            </a>
        );
    });
};
const CategorySelect = props => {
    return (
        <div className="dropdown-menu">
            <h6 className="dropdown-header">Select Category</h6>
            <a
                key="ALL"
                href="#answer"
                className="dropdown-item"
                onClick={() => props.onSelectCategory()}
            >
                ALL
            </a>
            <Categories
                categories={props.categories}
                onSelectCategory={props.onSelectCategory}
            />
        </div>
    );
};

const QuestionDetail = props => {
    const { question } = props;
    return (
        <tr>
            <td />
            <td colSpan="4" style={{ padding: 0 }}>
                <div
                    className="table-responsive"
                    style={{ overflowX: "initial" }}
                >
                    <table className="table table-hover" style={{ margin: 0 }}>
                        <tbody>
                            {question.image && (
                                <tr>
                                    <td colSpan="2">
                                        <img
                                            src={question.image}
                                            alt=""
                                            width="100%"
                                        />
                                    </td>
                                </tr>
                            )}

                            {question.choices.map(choice => (
                                <tr key={choice._id}>
                                    <td
                                        className="text-left"
                                        style={{ width: "100%" }}
                                    >
                                        {choice.choice}
                                    </td>
                                    <td>
                                        <div className="form-check">
                                            <label className="form-check-label">
                                                <input
                                                    className="form-check-input"
                                                    type="checkbox"
                                                    checked={
                                                        choice._id ===
                                                        question.answer
                                                    }
                                                    readOnly={true}
                                                />
                                                <span className="form-check-sign">
                                                    <span className="check" />
                                                </span>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
    );
};

const Questions = props => {
    return props.questions.map((question, index) => [
        <tr key={question._id}>
            <td className="text-center">{index + 1}</td>
            <td className="text-left">{question.question}</td>
            <td className="text-left">{question.category}</td>
            <td className="text-right">{question.timeLimit}</td>

            <td className="td-actions text-right">
                <button
                    type="button"
                    title="View Detail"
                    className="btn btn-info btn-link btn-just-icon btn-sm"
                    onClick={() => {
                        props.selectQuestion(question._id);
                    }}
                >
                    <i className="material-icons">format_list_bulleted</i>
                </button>
                <button
                    type="button"
                    title="Edit"
                    className="btn btn-warning btn-link btn-just-icon btn-sm"
                    onClick={() => {
                        props.editQuestion(question._id);
                    }}
                >
                    <i className="material-icons">edit</i>
                </button>
                <button
                    type="button"
                    title="Delete"
                    className="btn btn-danger btn-link btn-just-icon btn-sm"
                    onClick={() => props.deleteQuestion(question._id)}
                >
                    <i className="material-icons">close</i>
                </button>
            </td>
        </tr>,
        question._id === props.selectedQuestion && (
            <QuestionDetail
                key={question._id + "_detail"}
                question={question}
            />
        )
    ]);
};

class ViewQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            category: "",
            questions: [],
            loading: true
        };
    }

    componentDidMount() {
        this.fetchQuestions();
    }

    fetchQuestions = () => {
        this.setState({ loading: true });
        const category = this.state.category || "";
        const url = category ? "category/" + category : "";
        adminApiCaller(`admin/question/${url}`, "GET")
            .then(response => {
                if (category === this.state.category) {
                    const { questions } = response.data;
                    this.setState({
                        questions,
                        loading: false
                    });
                }
            })
            .catch(err => {
                console.log(err);
            });
    };

    onHandleChange = e => {
        let name = e.target.name;
        let value = e.target.value;
        this.setState({
            [name]: value
        });
    };

    selectQuestion = id => {
        const { selectedQuestion } = this.state;
        if (id === selectedQuestion) {
            this.setState({ selectedQuestion: null });
        } else {
            this.setState({ selectedQuestion: id });
        }
    };

    editQuestion = id => {
        this.props.history.push({
            pathname: "/admin/question/edit/" + id
        });
    };

    deleteQuestion = id => {
        if (window.confirm("Are you sure?") && id) {
            adminApiCaller(`admin/question/${id}`, "DELETE")
                .then(response => this.fetchQuestions())
                .catch(err => {
                    console.log(err);
                });
        }
    };

    onSelectCategory = category => {
        const selectedCategory = category || "";
        this.setState({ category: selectedCategory }, () =>
            this.fetchQuestions()
        );
    };

    render() {
        const { category, questions, loading } = this.state;
        return (
            <div className="tab-pane active">
                <div className="card-body card-search">
                    <button
                        className="dropdown-toggle form-control"
                        data-toggle="dropdown"
                        style={{
                            textAlign: "left"
                        }}
                    >
                        {category ? category : "ALL"}
                    </button>
                    <CategorySelect
                        categories={categories}
                        onSelectCategory={this.onSelectCategory}
                    />
                </div>
                <div className="card-body">
                    <ClipLoader
                        sizeUnit={"px"}
                        size={150}
                        color={"#123abc"}
                        loading={loading}
                    />
                    <div className="table-responsive" hidden={loading}>
                        <table className="table">
                            <thead>
                                <tr>
                                    <th className="text-center">#</th>
                                    <th>Question</th>
                                    <th className="text-left">Category</th>
                                    <th className="text-right">Time limit</th>
                                    <th className="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <Questions
                                    questions={questions}
                                    deleteQuestion={this.deleteQuestion}
                                    editQuestion={this.editQuestion}
                                    selectQuestion={this.selectQuestion}
                                    selectedQuestion={
                                        this.state.selectedQuestion
                                    }
                                />
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default ViewQuestion;
