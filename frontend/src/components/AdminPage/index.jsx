import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import CheckReward from "./CheckReward";
import ViewQuestion from "./ViewQuestion";
import ViewResult from "./ViewResult";
import AddQuestion from "./AddQuestion";
import apiCaller, { adminApiCaller } from "./../../utils/ApiCaller";

const routes = [
    {
        path: "/admin/checkreward",
        title: "Check Reward",
        component: CheckReward
    },
    {
        path: "/admin/question",
        title: "View Questions",
        component: ViewQuestion,
        exact: true
    },
    {
        path: "/admin/question/add",
        title: "Add Question",
        component: AddQuestion
    },
    {
        path: "/admin/result",
        title: "View Result",
        component: ViewResult
    }
];

const AdminRoutes = props => {
    return (
        <ul className="nav nav-pills nav-pills-icons">
            {props.routes.map(route => (
                <Route
                    key={route.path}
                    path={route.path}
                    exact={route.exact}
                    children={({ match }) => (
                        <li key={route.path} className="nav-item">
                            <Link
                                className={`nav-link ${match ? "active" : ""}`}
                                to={route.path}
                            >
                                {route.title}
                            </Link>
                        </li>
                    )}
                />
            ))}
        </ul>
    );
};

class AdminPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loggedIn: false,
            password: "",
            error: false
        };
    }

    componentWillMount() {
        adminApiCaller(`admin/login`, "GET")
            .then(response => {
                this.setState({
                    loggedIn: true,
                    error: false
                });
            })
            .catch(err => {});
    }

    onHandleChange = e => {
        let name = e.target.name;
        let value = e.target.value;
        this.setState({
            [name]: value
        });
    };

    onLogin = e => {
        e.preventDefault();
        const data = {
            password: this.state.password
        };
        apiCaller(`admin/login`, "POST", data)
            .then(response => {
                this.setState({
                    password: "",
                    loggedIn: true,
                    error: false
                });
                localStorage.setItem("token_admin", response.data.token);
            })
            .catch(err => {
                this.setState({
                    password: "",
                    error: true
                });
            });
    };

    render() {
        const { loggedIn, password, error } = this.state;

        return (
            <div className="container">
                <div className="col-xs-4 col-md-12 ml-auto mr-auto">
                    <div className="card text-center ">
                        <div className="card-header card-header-info text-center">
                            <h4 className="card-title">Admin Panel</h4>
                        </div>

                        <div className="card-body justify-content-center">
                            {!loggedIn && (
                                <form
                                    className="col-lg-4 ml-auto mr-auto justify-content-center"
                                    onSubmit={this.onLogin}
                                >
                                    <div className="form-group">
                                        <input
                                            type="password"
                                            className="form-control"
                                            placeholder="Password"
                                            name="password"
                                            value={password}
                                            onChange={this.onHandleChange}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <button className="btn btn-success btn-round">
                                            Login
                                        </button>
                                    </div>
                                </form>
                            )}
                            {loggedIn && (
                                <div className="row">
                                    <div className="col-md-12">
                                        <AdminRoutes routes={routes} />
                                        <hr />
                                        <div className="tab-content">
                                            <Switch>
                                                {routes.map((route, index) => {
                                                    return (
                                                        <Route
                                                            key={index}
                                                            path={route.path}
                                                            component={
                                                                route.component
                                                            }
                                                            exact={route.exact}
                                                        />
                                                    );
                                                })}
                                                <Route
                                                    key={"edit-question"}
                                                    path={
                                                        "/admin/question/edit/:id"
                                                    }
                                                    component={AddQuestion}
                                                />
                                            </Switch>
                                        </div>
                                    </div>
                                </div>
                            )}
                        </div>
                        <div className="alert alert-danger" hidden={!error}>
                            <div className="container">
                                <div className="alert-icon">
                                    <i className="material-icons">
                                        error_outline
                                    </i>
                                </div>
                                <b>Error:</b> Wrong password!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AdminPage;
