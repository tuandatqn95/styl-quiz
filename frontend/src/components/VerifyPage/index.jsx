import React, { Component } from "react";
import { ClipLoader } from "react-spinners";
import apiCaller from "../../utils/ApiCaller";

class VerifyPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            success: false,
            error: false,
            user: null
        };
    }

    componentDidMount() {
        const { userId } = this.props.match.params;
        apiCaller(`verify/${userId}`, "GET")
            .then(response => {
                this.setState({
                    loading: false,
                    success: true,
                    user: response.data.user
                });
                localStorage.setItem("token", response.data.token);
            })
            .catch(err => {
                this.setState({
                    loading: false,
                    error: true
                });
                console.log(err);
            });
    }

    playGame = () => {
        if (this.state.user && this.state.success) {
            this.props.history.push({
                pathname: "/quiz",
                state: {
                    fromVerify: true
                }
            });
        }
    };

    render() {
        const { user, success } = this.state;
        return (
            <div className="container">
                <div className="col-md-4 ml-auto mr-auto">
                    <div className="card card-profile text-center">
                        <div className="card-body ">
                            <h4 className="card-title">
                                {`Verify Account${
                                    success ? " Successful" : ""
                                }`}
                            </h4>
                            <ClipLoader
                                sizeUnit={"px"}
                                size={150}
                                color={"#123abc"}
                                loading={this.state.loading}
                            />
                        </div>
                        <div
                            className="alert alert-success"
                            hidden={!this.state.success}
                        >
                            <div className="container">
                                <div className="alert-icon">
                                    <i className="material-icons">check</i>
                                </div>
                                Your account was activated!
                            </div>
                        </div>
                        {user && [
                            <div key="card-body" className="card-body">
                                <h3 className="card-title">
                                    Welcome, {user.name}
                                </h3>
                                <h5 className="card-description text-justify">
                                    Thank you for joining our quiz. Click{" "}
                                    <b>START</b> button to answer the questions.
                                </h5>
                                <p className="text-danger">
                                    <em>
                                        Please note your account will be expired
                                        in 30 mins.
                                    </em>
                                </p>
                            </div>,
                            <div
                                key="card-footer"
                                className="card-footer justify-content-center"
                            >
                                <button
                                    className="btn btn-success btn-round"
                                    onClick={this.playGame}
                                >
                                    Start
                                </button>
                            </div>
                        ]}
                        <div
                            className="alert alert-danger"
                            hidden={!this.state.error}
                        >
                            <div className="container">
                                <div className="alert-icon">
                                    <i className="material-icons">
                                        error_outline
                                    </i>
                                </div>
                                <b>Error:</b> Cannot verify your account!
                                <br />
                                Please try again.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default VerifyPage;
