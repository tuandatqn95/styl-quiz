import React from "react";
import ReactDOM from "react-dom";

import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import { BrowserRouter } from "react-router-dom";

const appRoot = document.getElementById("root");
appRoot.setAttribute("notranslate", true);

ReactDOM.render(
    <BrowserRouter basename="app">
        <App />
    </BrowserRouter>,
    appRoot
);
registerServiceWorker();
