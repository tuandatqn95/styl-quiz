import http from "http";
import app from "./app";

const port = process.env.PORT || 4000;

const server = http.createServer(app);

server.listen(port);
server.on("error", err => {
    console.log(err.message);
});
server.on("listening", () => {
    console.log("Express is listening on " + port);
});
