import nodemailer from "nodemailer";
import config from "./../config";
import ejs from "ejs";

const transporter = nodemailer.createTransport({
    service: config.mailer.service,

    auth: {
        user: config.mailer.username,
        pass: config.mailer.password
    }
});

const verifyLinkTemplate = (name, id) => {
    return ejs.render(
        `Dear <%= name %>,<br/>
        <br/>
        Thanks for joining our quiz.<br/>
        Please click the link below to submit your answer for the questionnaire and earn reward.<br/>
        <br/>
        <%= verifyLink %><br/>
        <br/>
        Good luck,`,
        {
            name,
            verifyLink: config.base_url + "/app/verify/" + id,
            totalQuestion: config.question.totalQuestions
        }
    );
};

const rewardTemplate = (name, rightAnswer, giftCode, giftType) => {
    return ejs.render(
        `Dear <%= name %>,<br/>
        <br/>
        Thanks again for joining our quiz. You answered correctly <b><%= rightAnswer %>/<%= totalQuestion %></b> questions.<br/>
        You're rewarded a <b><%= giftType %></b> gift. Below is your gift code<br/>
        <br/>
        <b><%= giftCode %></b><br/>
        <br/>
        Please show the gift code to STYL staff to receive your gift.<br/>
        <br/>
        Have a pleasant day,`,
        {
            name,
            rightAnswer,
            giftCode,
            giftType,
            totalQuestion: config.question.totalQuestions
        }
    );
};

const failResultTemplate = (name, rightAnswer) => {
    return ejs.render(
        `Dear <%= name %>,<br/>
        <br/>
        Thanks again for joining our quiz. You answered correctly <b><%= rightAnswer %>/<%= totalQuestion %></b> questions.<br/>
        We're so sorry that your result has not meet condition to get a reward.<br/>
        Wish you a better luck next time.<br/>
        <br/>
        Have a pleasant day,`,
        {
            name,
            rightAnswer,
            totalQuestion: config.question.totalQuestions
        }
    );
};

const sendEmail = (email, subject, template) => {
    if (!config.sendEmail) {
        return;
    }
    return transporter.sendMail({
        from: "no-reply@styl.solutions",
        subject: subject,
        to: email,
        html: template
    });
};

exports.sendVerifyLink = (email, name, id) => {
    return sendEmail(
        email,
        "STYL Solutions quiz registration confirmation",
        verifyLinkTemplate(name, id)
    );
};

exports.sendReward = (email, name, rightAnswer, giftCode, giftType) => {
    return sendEmail(
        email,
        "STYL Solutions quiz result",
        rewardTemplate(name, rightAnswer, giftCode, giftType)
    );
};

exports.sendFailResult = (email, name, rightAnswer) => {
    return sendEmail(
        email,
        "Thanks for joining our quiz",
        failResultTemplate(name, rightAnswer)
    );
};
