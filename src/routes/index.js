import express from "express";
const appRouter = express.Router();
import multer from "multer";
import auth, { adminAuth } from "../middlewares/authMiddleware";
import UserController from "../controllers/UserController";
import QuestionController from "../controllers/QuestionController";
import RewardController from "../controllers/RewardController";
import config from "../config";
import jwt from "jsonwebtoken";

// File handling
const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, "./public/images");
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + "-" + file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    // reject a file
    if (["image/jpeg", "image/png", "image/gif"].includes(file.mimetype)) {
        cb(null, true);
    } else {
        cb(null, false);
    }
};
const upload = multer({
    storage: storage,
    limits: { fileSize: 1024 * 1024 * 2 },
    fileFilter: fileFilter
});

// Auth routes
appRouter.post("/register", UserController.register);
appRouter.post("/resend", UserController.resendEmail);
appRouter.get("/verify/:id", UserController.verify);

//Admin routes
appRouter.get("/admin/login", adminAuth, (req, res, next) => {
    res.send();
});
appRouter.post("/admin/login", (req, res, next) => {
    const password = req.body.password;
    if (password === config.adminPassword) {
        const token = jwt.sign({}, config.jwt.secretKeyAdmin, {
            expiresIn: config.jwt.expiresInAdmin
        });
        return res.json({ token });
    }
    return res.status(401).json({ message: "Wrong password" });
});

appRouter.get("/admin/update", RewardController.updateRewardCollection);
appRouter.post(
    "/admin/question",
    upload.single("image"),
    QuestionController.addQuestion
);
appRouter.put(
    "/admin/question/:id",
    upload.single("image"),
    QuestionController.editQuestion
);

appRouter.delete("/admin/question/:id", QuestionController.deleteQuestion);
appRouter.get("/admin/question", QuestionController.getAllQuestion);
appRouter.get("/admin/question/:id", QuestionController.getQuestionById);
appRouter.get(
    "/admin/question/category/:category",
    QuestionController.getQuestionByCategory
);
appRouter.get("/admin/result", RewardController.getResult);

// Question & Answer routes
appRouter.get("/question", auth, QuestionController.getQuestion);

appRouter.put("/answer/:id/:choiceId", auth, QuestionController.answerQuestion);
appRouter.put("/answer/:id/", auth, QuestionController.accessQuestion);

// Reward routes
appRouter.post("/result/", auth, RewardController.submitResult);
appRouter.get("/reward/:code", RewardController.getReward);
appRouter.put("/reward/:code", RewardController.confirmReward);

export default appRouter;
