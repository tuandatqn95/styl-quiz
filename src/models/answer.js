import mongoose from "mongoose";
const Schema = mongoose.Schema;

const answerSchema = new Schema(
    {
        user: { type: Schema.Types.ObjectId, required: true, ref: "User" },
        question: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: "Question"
        },
        answer: { type: Schema.Types.ObjectId },
        correct: { type: Schema.Types.Boolean, require: true, default: null }
    },
    {
        collection: "answer",
        timestamps: { createdAt: true, updatedAt: true }
    }
);

export default mongoose.model("Answer", answerSchema);
