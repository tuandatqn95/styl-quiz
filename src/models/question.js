import mongoose from "mongoose";
const Schema = mongoose.Schema;

const choiceSchema = new Schema({
    choice: { type: String, required: true }
});

const questionSchema = new Schema(
    {
        question: { type: String, required: true },
        answer: { type: Schema.Types.ObjectId },
        category: {
            type: String,
            required: true,
            enum: ["GENERAL", "EMBEDDED", "MOBILE", "SERVER", "HARDWARE"],
            default: "GENERAL"
        },
        choices: [{ type: choiceSchema }],
        image: { type: String },
        timeLimit: { type: Number, require: true, default: 0 }
    },
    {
        collection: "question",
        timestamps: { createdAt: true }
    }
);

export default mongoose.model("Question", questionSchema);
