import mongoose from "mongoose";
const Schema = mongoose.Schema;

const rewardSchema = new Schema(
    {
        user: { type: Schema.Types.ObjectId, required: true, ref: "User" },
        numberOfCorrect: { type: Number, required: true, default: 0 },
        giftCode: { type: String, trim: true, uppercase: true },
        rewardType: {
            type: String,
            enum: ["NONE", "STANDARD", "PREMIUM"],
            default: "NONE"
        },
        received: { type: Schema.Types.Boolean, required: true, default: false }
    },
    {
        collection: "reward",
        timestamps: {
            createdAt: true,
            updatedAt: true
        }
    }
);

export default mongoose.model("Reward", rewardSchema);
