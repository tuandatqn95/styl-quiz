import mongoose from "mongoose";
const Schema = mongoose.Schema;

const userSchema = new Schema(
    {
        email: { type: String, required: true, unique: true, trim: true },
        name: { type: String, required: true, trim: true },
        phone: { type: String, required: true, trim: true },
        university: { type: String, trim: true },
        faculty: { type: String, trim: true },
        major: {
            type: String,
            required: true,
            enum: ["EMBEDDED", "MOBILE", "SERVER", "HARDWARE"],
            default: "SERVER"
        },
        expectedGraduation: { type: String, trim: true },
        token: { type: String },
        played: { type: Schema.Types.Boolean, required: true, default: false }
    },
    {
        collection: "user",
        timestamps: { createdAt: true }
    }
);

export default mongoose.model("User", userSchema);
