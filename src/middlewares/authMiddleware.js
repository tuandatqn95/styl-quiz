import jwt from "jsonwebtoken";
import config from "./../config";

export default (req, res, next) => {
    const authorization = req.headers.authorization || "";
    const token = authorization.split(" ", 2)[1];

    jwt.verify(token, config.jwt.secretKey, (err, decode) => {
        if (err) {
            return res.status(401).json({ message: err.name });
        }
        req.payload = decode;
        next();
    });
};

export const adminAuth = (req, res, next) => {
    const authorization = req.headers.authorization || "";
    const token = authorization.split(" ", 2)[1];

    jwt.verify(token, config.jwt.secretKeyAdmin, (err, decode) => {
        if (err) {
            return res.status(401).json({ message: err.name });
        }
        req.payload = decode;
        next();
    });
};
