import Question from "../models/question";
import Answer from "../models/answer";
import Reward from "../models/reward";
import User from "../models/user";
import config from "../config";
import { sendReward, sendFailResult } from "./../utils/mailer";

const submitResult = async (req, res, next) => {
    const payload = req.payload;

    try {
        const user = await User.findById(payload._id).exec();
        if (!user) {
            return next("User not found");
        }
        if (user.played) {
            return res.status(400).json({
                message: "You already submit your result before"
            });
        }

        // Count correct answer to create rewardType
        const numberOfCorrect = await Answer.countDocuments({
            user: user._id,
            correct: true
        });
        let reward;
        if (numberOfCorrect >= config.question.premiumReward) {
            reward = await Reward.create({
                user: user._id,
                numberOfCorrect: numberOfCorrect,
                rewardType: "PREMIUM",
                giftCode: Math.random()
                    .toString(36)
                    .slice(-8)
            });
        } else if (numberOfCorrect >= config.question.standardReward) {
            reward = await Reward.create({
                user: user._id,
                numberOfCorrect: numberOfCorrect,
                rewardType: "STANDARD",
                giftCode: Math.random()
                    .toString(36)
                    .slice(-8)
            });
        } else {
            reward = await Reward.create({
                user: user._id,
                numberOfCorrect: numberOfCorrect
            });
        }

        // set user already had a reward
        user.played = true;
        await user.save();

        // Send email
        if (!reward.rewardType || reward.rewardType === "NONE") {
            await sendFailResult(user.email, user.name, reward.numberOfCorrect);
        } else {
            await sendReward(
                user.email,
                user.name,
                reward.numberOfCorrect,
                reward.giftCode,
                reward.rewardType
            );
        }

        // Return the result
        const result = `${numberOfCorrect}/${config.question.totalQuestions}`;
        return res.json({ result });
    } catch (e) {
        console.log(e);
        next(e);
    }
};

const getReward = (req, res, next) => {
    const code = req.params.code;
    Reward.findOne({ giftCode: code })
        .populate("user")
        .exec()
        .then(reward => {
            if (!reward) {
                return res.status(404).json({ message: "Reward not found" });
            }
            return res.json(reward);
        })
        .catch(err => {
            console.log(err);
            return next(err);
        });
};

const confirmReward = (req, res, next) => {
    const code = req.params.code;
    Reward.findOneAndUpdate(
        { giftCode: code },
        { received: true },
        { new: true }
    )
        .populate("user")
        .exec()
        .then(reward => {
            if (!reward) {
                return res.status(404).json({ message: "Reward not found" });
            }
            return res.json(reward);
        })
        .catch(err => {
            console.log(err);
            return next(err);
        });
};

const getResult = (req, res, next) => {
    Reward.find()
        .populate("user")
        .exec()
        .then(results => res.json({ results }))
        .catch(err => next(err));
};

const updateRewardCollection = (req, res, next) => {
    Answer.aggregate([
        {
            $match: { correct: true }
        },
        {
            $group: {
                _id: "$user",
                numberOfCorrect: { $sum: 1 }
            }
        }
    ]).then(answers => {
        answers.forEach(answer => {
            Reward.findOneAndUpdate(
                { user: answer._id },
                { numberOfCorrect: answer.numberOfCorrect }
            )
                .exec()
                .then(rewards => {
                    return res.json({ message: "Updated successfully" });
                })
                .catch(err => {
                    console.log(err);
                    return res.status(400).json({ message: "Updated fail" });
                });
        });
    });
};

export default {
    submitResult,
    getReward,
    getResult,
    confirmReward,
    updateRewardCollection
};
