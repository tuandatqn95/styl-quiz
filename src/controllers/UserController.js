import User from "./../models/user";
import Question from "../models/question";
import Answer from "../models/answer";
import jwt from "jsonwebtoken";
import config from "./../config";
import { sendVerifyLink } from "./../utils/mailer";

const register = async (req, res, next) => {
    const newUser = new User(req.body);

    try {
        const existUser = await User.findOne({ email: newUser.email }).exec();
        if (existUser) {
            return res
                .status(400)
                .json({ message: "This email address is already registered" });
        }

        await newUser.save();
        await sendVerifyLink(newUser.email, newUser.name, newUser._id);
        return res.status(201).json();
    } catch (e) {
        console.log(e);
        return next(e);
    }
};

const resendEmail = async (req, res, next) => {
    const email = req.body.email;
    try {
        const user = await User.findOne({ email }).exec();
        if (!user) {
            return res
                .status(400)
                .json({ message: "This email address is not exist" });
        }
        await sendVerifyLink(user.email, user.name, user._id);
        return res.send();
    } catch (e) {
        console.log(e);
        return next(e);
    }
};

// Promise
const getQuestion = (user, major, numberOfQuestions) =>
    new Promise((resolve, reject) => {
        Question.aggregate([
            { $match: { category: major } },
            { $sample: { size: numberOfQuestions } }
        ]).then(questions => {
            //Create default answer
            const _answers = questions.map(
                question =>
                    new Answer({
                        question: question._id,
                        user: user._id
                    })
            );
            Answer.insertMany(_answers, err => {
                if (err) {
                    return reject(err);
                }
                return resolve(_answers);
            });
        });
    });

//Verify and pick question
const verify = (req, res, next) => {
    let userId = req.params.id;
    User.findById(userId)
        .exec()
        .then(user => {
            if (!user) {
                return res.status(404).json({ message: "User not found!" });
            }
            if (user.token) {
                return new Promise((resolve, reject) => {
                    jwt.verify(user.token, config.jwt.secretKey, err => {
                        if (err) reject(err);
                        resolve(user);
                    });
                });
            }

            // Create token
            const payload = {
                _id: user._id,
                email: user.email,
                name: user.name,
                major: user.major
            };
            const token = jwt.sign(payload, config.jwt.secretKey, {
                expiresIn: config.jwt.expiresIn
            });
            user.token = token;
            return new Promise((resolve, reject) => {
                user.save()
                    .then(user => resolve(user))
                    .catch(err => reject(err));
            });
        })
        .then(user => {
            //Pick question if user is new
            return new Promise((resolve, reject) => {
                Answer.countDocuments({
                    user: user._id
                })
                    .exec()
                    .then(count => {
                        return resolve({ user, count });
                    })
                    .catch(err => reject(err));
            });
        })
        .then(result => {
            const { user } = result;

            if (result.count > 0) {
                return Promise.all([user]);
            }

            const numberOfQuestions = config.question.questionForEachMajor;
            return Promise.all([
                user,
                getQuestion(user, "GENERAL", numberOfQuestions),
                getQuestion(user, user.major, numberOfQuestions)
            ]);
        })
        .then(result => {
            const user = result[0];
            const token = user.token;
            return res.json({ user, token });
        })
        .catch(err => {
            console.log(err);
            return next(err);
        });
};

export default {
    register,
    verify,
    resendEmail
};
