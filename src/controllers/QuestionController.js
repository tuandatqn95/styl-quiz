import Question from "../models/question";
import Answer from "../models/answer";
import Reward from "../models/reward";
import User from "../models/user";
import config from "../config";

const addQuestion = (req, res, next) => {
    console.log("file", req.file);
    const image = req.file ? "/images/" + req.file.filename : null;
    const jsonData = JSON.parse(req.body.data);

    const data = {
        ...jsonData,
        image
    };

    const newQuestion = new Question(data);

    const rightChoice = data.choices.find(choice => choice.right === true);

    if (rightChoice) {
        newQuestion.answer = newQuestion.choices.find(
            choice => choice.choice === rightChoice.choice
        )._id;
    }

    newQuestion
        .save()
        .then(question => res.json(question))
        .catch(err => next(err));
};

const editQuestion = (req, res, next) => {
    const id = req.params.id;
    const image = req.file ? "/images/" + req.file.filename : null;
    const jsonData = JSON.parse(req.body.data);

    const data = {
        ...jsonData,
        image
    };

    const newQuestion = new Question(data);

    const rightChoice = data.choices.find(choice => choice.right === true);

    if (rightChoice) {
        newQuestion.answer = newQuestion.choices.find(
            choice => choice.choice === rightChoice.choice
        )._id;
    }

    let updateObj;
    if (image) {
        updateObj = {
            question: newQuestion.question,
            category: newQuestion.category,
            choices: newQuestion.choices,
            answer: newQuestion.answer,
            timeLimit: newQuestion.timeLimit,
            image
        };
    } else {
        updateObj = {
            question: newQuestion.question,
            category: newQuestion.category,
            choices: newQuestion.choices,
            answer: newQuestion.answer,
            timeLimit: newQuestion.timeLimit
        };
    }

    Question.findByIdAndUpdate(id, updateObj, { new: true })
        .exec()
        .then(question => {
            console.log(question);
            return res.json(question);
        })
        .catch(err => {
            console.log(err);
            return next(err);
        });
};

const deleteQuestion = (req, res, next) => {
    Question.findByIdAndRemove(req.params.id)
        .exec()
        .then(result => res.status(204).json(result))
        .catch(err => next(err));
};

const getAllQuestion = (req, res, next) => {
    Question.find()
        .sort({ category: 1 })
        .exec()
        .then(questions => res.json({ questions }))
        .catch(err => next(err));
};

const getQuestionById = (req, res, next) => {
    const id = req.params.id;
    Question.findById(id)
        .exec()
        .then(question => res.json({ question }))
        .catch(err => next(err));
};

const getQuestionByCategory = (req, res, next) => {
    const category = req.params.category.toUpperCase();
    Question.find({ category })
        .exec()
        .then(questions => res.json({ questions }))
        .catch(err => next(err));
};

const getQuestion = (req, res, next) => {
    const user = req.payload;

    Answer.find({
        user: user._id,
        correct: null
    })
        .select("question")
        .exec()
        .then(answers => {
            if (answers.length) {
                return res.status(200).json({
                    current:
                        config.question.totalQuestions - answers.length + 1,
                    total: config.question.totalQuestions,
                    questions: answers
                });
            }
            return res
                .status(400)
                .json({ message: "You already answered all questions." });
        })
        .catch(err => next(err));
};

const answerQuestion = async (req, res, next) => {
    const answerId = req.params.id;
    const choiceId = req.params.choiceId;
    const user = req.payload;

    try {
        const answer = await Answer.findById(answerId)
            .populate("question")
            .exec();

        if (answer.user.toString() !== user._id) {
            return res
                .status(403)
                .json("Don't have permission to update answer");
        }

        const timeSpent = new Date().getTime() - answer.updatedAt.getTime();
        const timeLimit =
            (config.question.forceTime ||
                answer.question.timeLimit ||
                config.question.forceTime) * 1000;
        if (timeSpent <= timeLimit + config.question.delayTime) {
            answer.answer = choiceId;
            answer.correct = answer.question.answer.toString() === choiceId;
            await answer.save();
        }

        return res.send();
    } catch (e) {
        console.log(e);
        next(e);
    }
};

const accessQuestion = async (req, res, next) => {
    const answerId = req.params.id;
    const user = req.payload;

    try {
        const answer = await Answer.findById(answerId)
            .populate("question")
            .exec();
        if (answer.user.toString() !== user._id) {
            return res
                .status(403)
                .json("Don't have permission to update answer");
        }

        if (answer.correct === null) {
            answer.correct = false;
            await answer.save();
        }

        const timeLimit =
            config.question.forceTime ||
            answer.question.timeLimit ||
            config.question.defaultTime;

        answer.question.timeLimit = timeLimit;

        return res.json({
            question: answer.question
        });
    } catch (e) {
        console.log(e);
        next(e);
    }
};

export default {
    addQuestion,
    editQuestion,
    deleteQuestion,
    getAllQuestion,
    getQuestionById,
    getQuestionByCategory,
    getQuestion,
    answerQuestion,
    accessQuestion
};
