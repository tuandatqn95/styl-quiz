import express from "express";
const app = express();
import morgan from "morgan";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import cors from "cors";
import path from "path";
import config from "./config";

// Init Database
mongoose
    .connect(
        config.database.url,
        {
            user: config.database.username,
            pass: config.database.password,
            useNewUrlParser: true
        }
    )
    .then(conn => {
        console.log("Connected to database!");
    })
    .catch(err => {
        console.log("Mongoose:", err.message);
    });
mongoose.set("useCreateIndex", true);
mongoose.Promise = Promise;

// Log and parse body
app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//CORS handling
app.use(cors());

// Routes which handle requests
import routes from "./routes";
app.use("/api", routes);

app.use(express.static(path.join(__dirname, "/../", "frontend", "build")));
app.use("/images/*", (req, res, next) => {
    res.sendFile(path.join(__dirname, "public", req.baseUrl), () => next());
});
console.log(path.join(__dirname, "/../", "frontend", "build", "index.html"));

app.get("/*", (req, res) => {
    res.sendFile(
        path.join(__dirname, "/../", "frontend", "build", "index.html")
    );
});

app.use((req, res, next) => {
    const error = new Error("Not found!");
    error.status = 404;
    next(error);
});

app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
        message: err.message
    });
});

export default app;
