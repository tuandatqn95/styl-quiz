import "dotenv/config";

const BASE_URL = "https://styl-event.herokuapp.com";
const DELAY_TIME = 5 * 1000;

const config = {
    database: {
        url:
            process.env.MONGO_URL ||
            "mongodb://ds147592.mlab.com:47592/styl-event",
        username: process.env.MONGO_USERNAME || "styl",
        password: process.env.MONGO_PASSWORD || "styl1234"
    },

    mailer: {
        service: "gmail",
        username: process.env.EMAIL_USERNAME || "", // Set in Environment Variable - Required
        password: process.env.EMAIL_PASSWORD || "" // Set in Environment Variable - Required
    },
    jwt: {
        secretKey: process.env.JWT_SECRET || "S3cr3t_K3y",
        expiresIn: process.env.JWT_EXPIRED || "30m",
        secretKeyAdmin: process.env.JWT_SECRET_ADMIN || "S3cr3t_K3y_ADMIN",
        expiresInAdmin: process.env.JWT_EXPIRED_ADMIN || "30m"
    },

    question: {
        delayTime: DELAY_TIME,
        forceTime: process.env.FORCE_TIME
            ? parseInt(process.env.FORCE_TIME)
            : null,

        defaultTime: process.env.DEFAULT_TIME
            ? parseInt(process.env.DEFAULT_TIME)
            : 30,

        // questionForEachMajor: 10
        questionForEachMajor: process.env.QUESTIONS_FOREACH_MAJOR
            ? parseInt(process.env.QUESTIONS_FOREACH_MAJOR)
            : 10,

        // totalQuestions:20
        totalQuestions: process.env.TOTAL_QUESTIONS
            ? parseInt(process.env.TOTAL_QUESTIONS)
            : 20,

        // standardReward:16
        standardReward: process.env.STANDARD_REWARD
            ? parseInt(process.env.STANDARD_REWARD)
            : 16,

        // premiumReward:18
        premiumReward: process.env.PREMIUM_REWARD
            ? parseInt(process.env.PREMIUM_REWARD)
            : 18
    },

    adminPassword: process.env.ADMIN_PASSWORD || "STYLWONDER",

    sendEmail: process.env.NODE_ENV === "production" ? true : true,
    base_url:
        process.env.BASE_URL ||
        (process.env.NODE_ENV === "production"
            ? BASE_URL
            : "http://localhost:3000")
};

export default config;
